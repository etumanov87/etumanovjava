package hierarchy_inheritance;

public class Men extends Human{
    static boolean isStrong;
    boolean isAdult;

    public Men(int age, int weight, String name, String surname, boolean isAdult) {
        super(age, weight, name, surname);
        this.isAdult = isAdult;
    }
    public static void pumpMuscles(){
        isStrong = true;
    }
    @Override
    public boolean checkHappy() {
        if (isStrong) { isHappy = true;}
        return super.checkHappy();
    }

    @Override
    public void eat() {
        System.out.println("Кушать");
        weight++;
    }

    @Override
    public void sleep() {
        System.out.println("Спать...");
    }

    @Override
    public void runOnWork() {
        super.runOnWork();
    }

    @Override
    public void fulfillWork() {
        super.fulfillWork();
    }

    @Override
    public void driveCar() {
        super.driveCar();
    }

    @Override
    public void driveBoat() {
        System.out.println("Наслаждаться поездкой!!");;
    }
}
