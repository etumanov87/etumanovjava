package hierarchy_inheritance;

/**
 * Этот интерфес используется для описания возможностей Класс Human
 * выполнять какие-то действия, к примеру работать или играть и тд.
 */
public interface Workable {

    default void runOnWork() {
        System.out.println("Сопротивляться сну и идти, идти, идти...");
    }

    default void fulfillWork() {
        System.out.println("Делать работу");

    }

    default void play() {
        System.out.println("Играть");
    }

    default void washDishes(){

    }
    default void rest(){
        System.out.println("Отдыхать");
    }
}
