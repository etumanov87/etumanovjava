package hierarchy_inheritance;

/**
 * Это абстрактный класс, его мы используем для создания "заготовки" человека
 * Так же в этом класс происходит подклюения интерфейсов, так как все
 * подклассы смогуть выполнять эти действия
 */
public abstract class Human implements Workable, Drivable {
    int age;
    int weight;
    String name;
    String surname;
    boolean isHappy;


    public Human(int age, int weight, String name, String surname) {
        this.age = age;
        this.weight = weight;
        this.name = name;
        this.surname = surname;
    }

    public abstract void eat();

    public abstract void sleep();

    public boolean checkHappy(){
        return isHappy;
    }
}
