package hierarchy_inheritance;

public class Girl extends Women{
    public Girl(int age, int weight, String name, String surname, boolean isAdult) {
        super(age, weight, name, surname, isAdult);
    }

    @Override
    public void driveScooter() {
        super.driveScooter();
    }

    @Override
    public void sleep() {
        super.sleep();
        System.out.println("Капризничать");
    }

    @Override
    public void play() {
        super.play();
    }
}
