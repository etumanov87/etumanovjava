package hierarchy_inheritance;


public class Boy extends Men{


    public Boy(int age, int weight, String name, String surname, boolean isAdult) {
        super(age, weight, name, surname, isAdult);
    }

    @Override
    public boolean checkHappy() {
        return isHappy;
    }

    @Override
    public void driveScooter() {
        isHappy = true;
    }

    @Override
    public void eat() {
        super.eat();
        System.out.println("Капризничать");
    }

    @Override
    public void play() {
        super.play();
    }

    @Override
    public void rest() {
        super.rest();
    }
}
