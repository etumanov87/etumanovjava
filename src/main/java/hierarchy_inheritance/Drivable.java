package hierarchy_inheritance;

/**
 * Этот интерфес используется для описания возможностей Класс Human
 * управлять машиной, лодкрй или скутором
 */
public interface Drivable {

    default void driveCar() {
        System.out.println("Садимся,\n жмем на газ\n едем");
    }

    default void driveBoat() {

    }

    default void driveScooter() {

    }
}
