package hierarchy_inheritance;

public class Women extends Human{
    boolean isAdult;
    boolean isBeautiful;
    public Women(int age, int weight, String name, String surname, boolean isAdult) {
        super(age, weight, name, surname);
        this.isAdult = isAdult;
    }

    @Override
    public void eat() {
        System.out.println("Кушать и не полнеть");
    }

    @Override
    public void sleep() {
        System.out.println("Спать...");
    }

    @Override
    public void driveCar() {
        super.driveCar();
    }

    @Override
    public void washDishes() {
        super.washDishes();
    }

    @Override
    public boolean checkHappy() {
        return isHappy;
    }

    @Override
    public void rest() {
        isHappy = true
        ;
    }
}
