package hierarchy_inheritance;

public class Main {
    public static void main(String[] args) {
        Human girl = new Girl(14,35,"Masha", "Ivanova", false);
        girl.eat();
        girl.sleep();
        girl.play();

        Human men = new Men(35, 95, "Misha", "Petrov", true);
        men.eat();
        men.sleep();
        System.out.println(men.checkHappy());
        Men.pumpMuscles();
        System.out.println(men.checkHappy());
        men.driveBoat();
        men.fulfillWork();

        Human boy = new Boy(15, 50, "Pasha", "Smirov", false);
        boy.eat();
        boy.sleep();
        System.out.println(boy.checkHappy());
        boy.driveScooter();
        System.out.println(boy.checkHappy());

        Human women = new Women(32, 60, "Natasha", "Popova", true);
        women.eat();
        women.washDishes();
        women.driveCar();
        System.out.println(women.checkHappy());
        women.rest();
        System.out.println(women.checkHappy());
    }
}
