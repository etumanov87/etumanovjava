package hockeymatch;

import java.util.Arrays;

public class Billet {

    String fio;
    int numberBillet;
    String sector;
    int numberPlace;

    public Billet(String fio, int numberBillet, String sector, int numberPlace) {
        this.fio = fio;
        this.numberBillet = numberBillet;
        this.sector = sector;
        this.numberPlace = numberPlace;
    }

    @Override
    public String toString() {
        return "Billet{" +
                "fio='" + fio + '\'' +
                ", numberBillet=" + numberBillet +
                ", sector='" + sector + '\'' +
                ", numberPlace=" + numberPlace +
                "}\n";
    }

    public static void checkBillet(Billet[] billets, String[] sectors) {
        for (Billet billet : billets) {
            if (billet.fio.isEmpty()) {
                    System.out.println("Ваш билет не валидный, отсутсвует ФИО: " + billet.fio);

            } else if (!Arrays.asList(sectors).contains(billet.sector)){
                System.out.println("Ваш билет не валидный, не правильно указан сектор: " + billet.sector);
            }
            else {
                switch (billet.sector) {
                    case "A1", "A2" -> System.out.println("Просим сохранить билет до конца матча");
                    case "VIP" -> System.out.println("Добро пожаловать на матч! " + billet.fio);
                }
            }
        }
    }
}
