package hockeymatch;

import com.github.javafaker.Faker;

import java.util.Arrays;
import java.util.Random;

public class App {
    public static void main(String[] args) {
        Faker faker = new Faker();
        Random random = new Random();
        String[] listSector = {"A1","A2","A3","B1","B2", "B3", "VIP"};

        Billet[] listBillet = new Billet[20];
        listBillet[0] = new Billet("", 15, "A2", 1);
        listBillet[1] = new Billet("", 25,"89",10);
        listBillet[2] = new Billet("Scott Schiller", 25,"Errore",10);

        for (int i = 3; i < listBillet.length; i++) {
            listBillet[i] = new Billet(faker.name().fullName(), random.nextInt(200), listSector[random.nextInt(0,7)], random.nextInt(30));
        }

        Billet.checkBillet(listBillet, listSector);
    }

}
