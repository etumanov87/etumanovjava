package calculator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static calculator.Operation.*;

public class CalculationEvaluation {
    public static String checkExistsString(String expression) {
        if (expression.contains("плюс")) {
            return expression.replaceAll("плюс", "+");
        } else if (expression.contains("минус")) {
            return expression.replaceAll("минус", "-");
        } else if (expression.contains("умножить")) {
            return expression.replaceAll("умножить", "*");
        } else if (expression.contains("делить")) {
            return expression.replaceAll("делить", "/");
        } else {
            return expression;
        }
    }

    public static void parsingString(String expression) {
        expression = checkExistsString(expression);

        List<String> list;
        double res = 0;
        try {
            if (expression.contains("+")) {
                list = List.of(expression.trim().split("\\+"));
                res = sum(Double.parseDouble(list.get(0)), Double.parseDouble(list.get(1)));
            } else if (expression.contains("-")) {
                list = List.of(expression.trim().split("-"));
                res = subtract(Double.parseDouble(list.get(0)), Double.parseDouble(list.get(1)));
            } else if (expression.contains("*")) {
                list = List.of(expression.trim().split("\\*"));
                res = multiply(Double.parseDouble(list.get(0)), Double.parseDouble(list.get(1)));
            } else if (expression.contains("/")) {
                list = List.of(expression.trim().split("/"));
                if(Double.parseDouble(list.get(1)) == 0) {
                    //TODO throw System.out.println("На 0 делить нельзя!");
                }
                res = divide(Double.parseDouble(list.get(0)), Double.parseDouble(list.get(1)));
            }
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
            System.out.println(e.getMessage());
        }
        res = Math.ceil(res * 100) / 100;
        System.out.println("Результат вычисления: " + res);
    }

    public static void parsingString1(String expression) {
        expression = checkExistsString(expression);

        List<String> list;
        double res = 0;
        try {
            if (expression.contains("+")) {
                res = Stream.of(expression.trim().split("\\+"))
                        .mapToDouble(Double::parseDouble)
                        .sum();
            } else if (expression.contains("-")) {
                res = Stream.of(expression.trim().split("-"))
                        .mapToDouble(Double::parseDouble)
                        .reduce(Operation::subtract).getAsDouble();
            } else if (expression.contains("*")) {
                res = Stream.of(expression.trim().split("\\*"))
                        .mapToDouble(Double::parseDouble)
                        .reduce(Operation::multiply).getAsDouble();
            } else if (expression.contains("/")) {
                res = Stream.of(expression.trim().split("/"))
                        .mapToDouble(Double::parseDouble)
                        .reduce(Operation::divide).getAsDouble();
            }
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
            System.out.println(e.getMessage());
        }
        res = Math.ceil(res * 100) / 100;
        System.out.println("Результат вычисления: " + res);
    }
}
