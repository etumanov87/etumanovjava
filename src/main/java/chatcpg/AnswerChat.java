package chatcpg;

import java.util.Objects;
import java.util.Random;

public class AnswerChat {
    public static Random random = new Random();
    public static String[] optionAnswerOnDialogue = {"ясно", "понятно", "ок", ")"};
    public static String[] optionAnswerOnQuestion = {"не знаю", "хз"};

    public static void answerChat(String questionUser) {
        if (Objects.equals(questionUser, "")) {
            System.out.println("[ChanCPG]: Введите сообщение!");
        } else if (questionUser.charAt(questionUser.length() - 1) == '?') {
            System.out.printf("[ChanCPG]: %s\n", optionAnswerOnQuestion[random.nextInt(0, optionAnswerOnQuestion.length)]);
        } else {
            System.out.printf("[ChanCPG]: %s\n", optionAnswerOnDialogue[random.nextInt(0, optionAnswerOnDialogue.length)]);
        }
    }
}
