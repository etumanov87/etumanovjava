package chatcpg;

import java.util.Objects;
import java.util.Scanner;

import static chatcpg.AnswerChat.answerChat;

public class WorkChat {
    public static Scanner scanner = new Scanner(System.in);
    public static void workingChat() {
        while (true) {
            System.out.print("[Пользователь]: ");
            String questionUser = scanner.nextLine();
            if (Objects.equals(questionUser.toLowerCase(), "выход")) {
                System.out.println("Захочешь пообщаться, приходи!");
                break;
            }
            answerChat(questionUser);
        }
    }
}
