package inheritance;


public class Gasoline extends LiquidFuels {

    String brand;
    String manufacturer;

    public Gasoline(String typeFuel, String name, int price, double specificHeatCombustion, String brand, String manufacturer) {
        super(typeFuel, name, price, specificHeatCombustion);
        this.brand = brand;
        this.manufacturer = manufacturer;
    }

    public void optionGasoline(){
        System.out.printf("Параметры бензина: \n Производитель: %s,\n Марка %s,\n Цена %d\n", manufacturer, brand, price);
    }

    public int viewOctaneNumber(String brand) {
        return Integer.parseInt(brand.replaceAll("[^0-9]", ""));
    }


}
