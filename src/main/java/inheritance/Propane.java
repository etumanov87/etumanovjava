package inheritance;

public class Propane extends GaseousFuels {

    private int temperatureAutoIgnition = 466;
    private int temperatureWelding = 2400;

    public Propane(String typeFuel, String name, int price, double specificHeatCombustion) {
        super(typeFuel, name, price, specificHeatCombustion);
    }

    @Override
    public void burnFuel() {
        System.out.printf("Вы опасный человек, поджигаите %s!!!\n", name);
        ;
    }

    public void convertRefrigerant() {
        System.out.printf("%s переработан в Хладагент\n", name);

    }

    public void useWelding(int temperatureStart) {
        System.out.printf("Осторожно температура самовоспламенения %s = %d\n", name, temperatureAutoIgnition);
        while (temperatureStart <=  temperatureWelding) {
            temperatureStart = temperatureStart + 800;
            System.out.printf("Идет процесс нагрева для сварки...Температура нагрева: %d\n", temperatureStart);
        }
        System.out.println("Можно начинать сварку!");
    }
}
