package inheritance;


public class SolidFuels extends Fuel {

    public String[] typeSolidFuels = {"Уголь", "Кокс", "Порох", "Торф"};
    private static final String[] levelHeatCombustion = {"Низкий уровень.", "Средний уровень.", "Высокой уровень."};

    public SolidFuels(String typeFuel, String name, int price, double specificHeatCombustion) {
        super(typeFuel, name, price, specificHeatCombustion);
    }

    public void showLevelHeatCombustion() {
        System.out.println("Какой уровень теплоты сгорания:");
        if (specificHeatCombustion < 10) {
            System.out.println(levelHeatCombustion[0]);
        } else if (specificHeatCombustion >= 10 | specificHeatCombustion < 30) {
            System.out.println(levelHeatCombustion[1]);
        } else {
            System.out.println(levelHeatCombustion[2]);
        }
    }

    @Override
    public void burnFuel() {
        System.out.printf("%s %s загорелось!!\n", typeFuel, name);
    }
}
