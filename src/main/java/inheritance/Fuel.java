package inheritance;

public class Fuel {
    String name;
    String typeFuel;
    int price;
    double specificHeatCombustion;

    public Fuel(String typeFuel, String name, int price, double specificHeatCombustion) {
        this.typeFuel = typeFuel;
        this.name = name;
        this.price = price;
        this.specificHeatCombustion = specificHeatCombustion;

    }

    public void viewTypeFuels(String[] arrayTypeFuels) {
        System.out.printf("Виды. %s:\n", typeFuel);
        for (String type : arrayTypeFuels) {
            System.out.println(type);
        }
    }

    public void payFuel(int quantity) {
        int result = price * quantity;
        System.out.printf("Вы купили %s по такой цене: %d\n", name, result);
    }

    public void sellFuel(int quantity) {
        double result = price * 1.5 * quantity;

        System.out.printf("Вы продали %s по такой цене: %d\n", name, Math.round(result));
    }

    public void burnFuel() {
        System.out.println("Горит!");
    }

    @Override
    public String toString() {
        return "Fuel{" +
                "name='" + name + '\'' +
                ", typeFuel='" + typeFuel + '\'' +
                ", price=" + price +
                ", specificHeatCombustion=" + specificHeatCombustion +
                '}';
    }
}
