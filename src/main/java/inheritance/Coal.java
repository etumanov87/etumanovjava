package inheritance;

public class Coal extends SolidFuels {
    String typeCoal;
    boolean coalSeparated;

    public Coal(String typeFuel, String name, int price, double specificHeatCombustion, String typeCoal) {
        super(typeFuel, name, price, specificHeatCombustion);
        this.typeCoal = typeCoal;
    }

    public void coalPreparation() {
        System.out.printf("%s уголь обогащен!\n", typeCoal);
        coalSeparated = true;
    }

    public void isPreparation() {
        if (coalSeparated) {
            System.out.printf("Уголь обогащен? Ответ: %s\n", coalSeparated);
        } else {
            System.out.printf("Уголь обогащен? Ответ: %s\n", coalSeparated);
        }
    }
}

