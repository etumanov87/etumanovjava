package inheritance;

/**
 * Как я понимаю полиморфизм в Java.
 * Это возможность пользоваться переменнами и методами Классах родителя при использовании обектов дочерних классов
 * Так же возможность при объявлянии переменной типа Родидется присвоить ей значения одного и наследников
 * Пример:
 * Fuel coal = new Coal("Твердое топливо", "Уголь", 30, 29.7, "Древесный");
 *  в этом случае можно будет использовать толькометоды и переменный класс Fuel
 * Coal coal = new Coal("Твердое топливо", "Уголь", 30, 29.7, "Древесный");
 *  в этом случае можно будет использовать толькометоды и переменный класс Fuel, LiquidFuels и Coal
 */
public class App {
    public static void main(String[] args) {
        Fuel[] fuels = new Fuel[3];

        Coal coal = new Coal("Твердое топливо", "Уголь", 30, 29.7, "Древесный");
        fuels[0] = coal;

        coal.viewTypeFuels(coal.typeSolidFuels);
        coal.isPreparation();
        coal.coalPreparation();
        coal.isPreparation();
        coal.payFuel(120);
        coal.sellFuel(120);
        coal.showLevelHeatCombustion();
        coal.burnFuel();

        Gasoline gasoline = new Gasoline("Жидкое топливо", "Бензин", 100, 46.0, "АИ-95", "Лукойл");
        fuels[1] = gasoline;

        System.out.printf("Точно %s относится к виду %s? Ответ: %s\n", gasoline.name, gasoline.typeFuel, gasoline.checkLiquidFuels());
        gasoline.viewTypeFuels(gasoline.typeLiquidFuels);
        gasoline.optionGasoline();
        System.out.printf("Октановое число %s %s = %d\n", gasoline.name, gasoline.brand, gasoline.viewOctaneNumber(gasoline.brand));
        gasoline.payFuel(200);
        gasoline.sellFuel(150);
        System.out.printf("Количество канистр %s: %d шт.\n", gasoline.name, gasoline.pourIntoCans(78));
        gasoline.burnFuel();


        Propane propane = new Propane("Газообразное топливо", "Пропан", 15, 48.0);
        fuels[2] = propane;

        propane.viewTypeFuels(propane.typeGaseousFuels);
        propane.checkStateGaseousFuels();
        propane.convertLiquid();
        propane.checkStateGaseousFuels();
        propane.convertRefrigerant();
        propane.payFuel(45);
        propane.sellFuel(15);
        propane.burnFuel();
        propane.useWelding(25);

        for (Fuel fuel : fuels) {
            System.out.println(fuel);
        }


    }
}
