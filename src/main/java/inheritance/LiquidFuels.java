package inheritance;

public class LiquidFuels extends Fuel {

    public String[] typeLiquidFuels = {"Бензин", "Керосин", "Мазут", "Дизель", "Спирт"};

    private boolean isLiquidFuels;

    int volumeCans = 20;


    public LiquidFuels(String typeFuel, String name, int price, double specificHeatCombustion) {
        super(typeFuel, name, price, specificHeatCombustion);
    }

    public boolean checkLiquidFuels() {
        for (String typeFuel : typeLiquidFuels) {
            if (typeFuel.equals(name)) {
                isLiquidFuels = true;
                break;
            }
        }
        return isLiquidFuels;
    }

    public int pourIntoCans(int volumeFuel) {
        return (volumeFuel / volumeCans);
    }

}
