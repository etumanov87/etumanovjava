package inheritance;

public class GaseousFuels extends Fuel {

    public String[] typeGaseousFuels = {"Водород", "Газ", "Пропан"};

    boolean isStateGaseousFuels;

    public GaseousFuels(String typeFuel, String name, int price, double specificHeatCombustion) {
        super(typeFuel, name, price, specificHeatCombustion);
    }

    public void convertLiquid() {
        System.out.println("Произведена обработка в жидкое состояние");
        isStateGaseousFuels = true;
    }

    public void checkStateGaseousFuels () {
        if (!isStateGaseousFuels) {
            System.out.println("Газообразное состояние");
        } else {
            System.out.println("Жидкое состояние");
        }
    }

}
