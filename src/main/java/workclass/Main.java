package workclass;

public class Main {
    public static void main(String[] args) {

        BankCard cardVisa = new BankCard();
        cardVisa.balance = 1000;
        cardVisa.nameOwner = "Alex";
        cardVisa.surnameOwner = "Ivanov";
        cardVisa.paySystem = "Visa";

        cardVisa.checkBalance();
        cardVisa.dataCard();
        cardVisa.isMoneyOnCard();
        cardVisa.paymentByCard(300);

        System.out.println("-------------------------------");

        BankCard cardMaster = new BankCard();
        cardMaster.balance = 0;
        cardMaster.nameOwner = "Bob";
        cardMaster.surnameOwner = "Malkin";
        cardMaster.paySystem = "MasterCard";

        cardMaster.checkBalance();
        cardMaster.dataCard();
        cardMaster.isMoneyOnCard();
        cardMaster.paymentByCard(250);

        System.out.println("-------------------------------");

        BankCard cardMir = new BankCard();
        cardMir.balance = 12000;
        cardMir.nameOwner = "Ivan";
        cardMir.surnameOwner = "Morozov";
        cardMir.paySystem = "Mir";

        cardMir.checkBalance();
        cardMir.dataCard();
        cardMir.isMoneyOnCard();
        cardMir.paymentByCard(2300);

    }
}
