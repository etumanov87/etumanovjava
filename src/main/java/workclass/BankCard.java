package workclass;

public class BankCard {
    int balance;
    String nameOwner;
    String surnameOwner;
    String paySystem;



    public void checkBalance() {
        System.out.printf("Баланс вашей карты: %d\n", balance);
    }

    public void dataCard() {
        System.out.printf("Владелец банковской карты: %s %s\n", nameOwner, surnameOwner);
        System.out.printf("Платежная система: %s\n", paySystem);
    }

    public void isMoneyOnCard() {
        if(balance == 0) {
            System.out.println("Денег нету");
        } else {
            System.out.println("Деньги есть");
        }
    }

    public void paymentByCard(int pricePurchase) {
        if(balance - pricePurchase < 0) {
            System.out.println("Вам не хватает денег!");
            checkBalance();
        } else {
            balance -= pricePurchase;
            System.out.printf("Вы совершили покупку на %d\n", pricePurchase);
            checkBalance();
        }
    }

}
