package shop;

public class App {
    public static void main(String[] args) {
        Plate plate1 = new Plate("Metallica", "Master of Puppets", 1986);
        Plate plate2 = new Plate("BoneyNEM", "Romantic Collection", 2003);
        Plate plate3 = new Plate("Megadeath", "Dystopia", 2015);
        Plate plate4 = new Plate("Avulsed", "Ritual Zombi ", 2013);
        Plate plate5 = new Plate("Amatory", "Doom", 2019);

        Store store = new Store("Москва, Новый Арбат, 10");

        store.showAmountRecordsInWarehouse();

        store.addRecordInWarehouse(plate1.toString());
        store.showAmountRecordsInWarehouse();

        store.addRecordInWarehouse(plate2.toString());
        store.showAmountRecordsInWarehouse();

        store.addRecordInWarehouse(plate3.toString());
        store.showAmountRecordsInWarehouse();

        store.addRecordInWarehouse(plate4.toString());
        store.showAmountRecordsInWarehouse();

        store.addRecordInWarehouse(plate5.toString());
        store.showAmountRecordsInWarehouse();

        Plate.printCounterReleasedRecords();

    }
}
