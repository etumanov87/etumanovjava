package shop;

public class Plate {
    String group;
    String album;
    int year;
    static int counterReleasedRecords;

    public static void printCounterReleasedRecords() {
        System.out.printf("Количество выпущенных пластинок: %s шт.\n", counterReleasedRecords);
    }

    public Plate(String group, String album, int year) {
        this.group = group;
        this.album = album;
        this.year = year;
        counterReleasedRecords++;
    }


    @Override
    public String toString() {
        return "Plate {" + "group='" + group + '\'' + ", album='" + album + '\'' + ", year=" + year + '}';
    }
}
