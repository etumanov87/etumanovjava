package shop;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Store {
    String address;

    public Store(String address) {
        this.address = address;
    }

    private static final String[] warehouseRecords = new String[5];

    int placeWarehouse = 0;

    public void addRecordInWarehouse(String record) {
        if (warehouseRecords[placeWarehouse] != null) {
            placeWarehouse++;
        }
        warehouseRecords[placeWarehouse] = record;
    }

    public void showAmountRecordsInWarehouse() {
        System.out.printf("Какие пластинки есть на складе по адресу: %s\n", address);
        if (placeWarehouse == 0 && warehouseRecords[placeWarehouse] == null) {
            System.out.println("На складе нету пластинок!");

        } else {
            for (int i = 0; i < warehouseRecords.length; i++) {
                if(warehouseRecords[i] == null) {
                    warehouseRecords[i] = "Место на складе свободно.";
                }
                System.out.printf("%d %s\n", i + 1, warehouseRecords[i]);
            }
        }
    }

}